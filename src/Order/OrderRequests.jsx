
class OrderRequests {
    constructor(){}


    async listOrders(){
        return fetch('http://localhost:8000/pedidos/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
    
    async createOrder(order){
        return fetch('http://localhost:8000/pedidos/', {
            method: 'POST' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: order,
        });
    }
    
    async getOrder(id){
        return fetch('http://localhost:8000/pedidos/' + id, {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }

    async updateOrder(id, order){
        return fetch('http://localhost:8000/pedidos/' + id, {
            method: 'PUT' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: order,
        });
    }

    async closeOrder(id){
        return fetch('http://localhost:8000/pedidos/' + id, {
            method: 'DELETE' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
}


export default OrderRequests;