import React from 'react';
import { useState, useEffect } from 'react';
// import OrderRequests from '../OrderRequests';
import ProductRequests from '../../Products/ProductRequests';

const productRequest = new ProductRequests();

const ProductSelect = ({ handleProductChange }) => {
  const [products, setProducts] = useState([])
  useEffect(() => {
    productRequest.listProducts()
          .then((res) => res.json())
          .then((data) => {
            setProducts(data)
          })
  }, []);
  const handleSelectChange = (e) => {
    // Aquí debes obtener el objeto del producto seleccionado según su ID
    const productId = e.target.value;
    const product = getProductById(productId);

    handleProductChange(product);
  };

  const getProductById = (productId) => {
    // Aquí debes buscar y devolver el objeto del producto según su ID
    // Puedes utilizar la función Array.find() o cualquier otra lógica para buscar el producto en tus datos de productos
    
    return products.find((product) => product.id == productId);
  };

  return (
    <div>
      <select onChange={handleSelectChange} className="form-select">
        <option value="">Selecciona un producto</option>
        {
          products.map((product)=>{
            return (
              <option value={product.id}>{product.nombre}</option>
            )
          })
        }
      </select>
    </div>
  );
};

export default ProductSelect;