import React from 'react';
import OrderForm from './Form/OrderForm';



const orderList = ({pedidos, onEdit, onDelete, onClickNewOrder}) => {
    const role = JSON.parse(window.localStorage.getItem('role'));
    const actionButton = (pedido) => {
        if( role == 'recepcion'){
            return <button className="btn btn-primary" onClick={() => onDelete(pedido.id)}>Pedido Entregado</button>
         }else {
             return <button className="btn btn-primary" onClick={() => onEdit(pedido)}>Pedido Preparado</button>
         }
    }

    const createButton = () => {
        if( role == 'recepcion'){
            return <button className="btn btn-primary" onClick={() => onClickNewOrder()}>Nuevo Pedido</button>
        }else{
            return <br/>
        }
    }
    
    return (
    <>
        
        <h2>Pedidos</h2>
        <div className="position-absolute start-10">{createButton()}</div>
        <br/><br/>
        <div key="pedidosAccordion" className="accordion" id="pedidos">
            {pedidos.map((pedido) => {
                let entrada = new Date(pedido.entrada);
                let targetAccordionButton = "#" + pedido.id;
                let productos = JSON.parse(pedido.productos);
                console.log(productos);
                return (<>
                    <div className="accordion-item">
                        <h2 className="accordion-header">
                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={targetAccordionButton} aria-expanded="false" aria-controls={pedido.id}>
                            {pedido.cliente} - {entrada.getUTCHours()}:{entrada.getMinutes()}:{entrada.getSeconds()}
                        </button>
                        </h2>
                        <div id={pedido.id} className="accordion-collapse collapse" data-bs-parent="#pedidos">
                            <div className="accordion-body">
                                <table className="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Producto</th>
                                        <th scope="col">Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {productos.map((producto) => {
                                        return (<>
                                        <tr key={producto.id}>
                                            <td>{producto.nombre}</td>
                                            <td>{producto.cantidad}</td>
                                        </tr>
                                            </>
                                        );
                                        
                                    })}
                                    </tbody>
                                </table>
                                {actionButton(pedido)}
                            </div>
                        </div>
                    </div>
                    </>
                );
            })}
        </div>
    </>
    );
};

export default orderList;