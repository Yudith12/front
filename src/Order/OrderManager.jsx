import React, { useState, useEffect } from 'react';
import OrderForm from './Form/OrderForm';
import OrderList from './OrderList';
import OrderRequests from './OrderRequests';
import axios from 'axios';

const orderRequest = new OrderRequests();

const OrderManager = () => {

  const url='http://localhost:8000/pedidos/';
  const role = JSON.parse(window.localStorage.getItem('role'));
  const [showOrderForm, setShowOrderForm] = useState(false);

  const handleClick = () => {
    setShowOrderForm(true);
  };

  const [pedidos, setPedidos] = useState([])

    useEffect(() => {
            orderRequest.listOrders()
            .then((res) => res.json())
            .then((data) => {
                setPedidos(data)
            })
    }, []);

  const  handleEditOrder = (order) => {
    console.log("EL prodcuto " + JSON.stringify(order));
    if (order != null && order != undefined){

      order.estado = "servir";
      const salida =  new Date(Date.now());
      order.salida = salida.toISOString()
      console.log("El order " + JSON.stringify(order));
      const respuesta = 
        axios({ 
        method:'PUT',
        url:url + order.id, 
        data: order,
        headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
        },
      })
      .then( (res) => console.log(res))
      .then(() => {{
          orderRequest.listOrders()
          .then((res) => res.json())
          .then((data) => {
              setPedidos(data)
          })
        }
      })
      .catch( (error) => console.error(error));
    }
  };

  const handleDeleteOrder = (productoId) => {
    if (productoId != null && productoId != undefined){
      orderRequest.closeOrder(productoId)
      .then((res)=> console.log(JSON.stringify(res)))
      .then( () =>{
          orderRequest.listOrders()
          .then((res) => res.json())
          .then((data) => {
              setPedidos(data)
          })
        }
      )
      .catch((err) => console.error(err));
    }
  };

  const content = (role === 'recepcion' && showOrderForm ) ? <OrderForm /> : <OrderList pedidos={pedidos} onClickNewOrder={handleClick} onEdit={handleEditOrder} onDelete={handleDeleteOrder} />;
  return (
    <div>
      {content}
    </div>
  );
};

export default OrderManager;
