
class ProductRequests {
    constructor(){}


    async listProducts(){
        return fetch('http://localhost:8000/productos/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
    
    async createProduct(product){
        return fetch('http://localhost:8000/productos/', {
            method: 'POST' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: product,
        });
    }
    
    async getProduct(id){
        return fetch('http://localhost:8000/productos/' + id, {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }

    async updateProduct(id, product){
        return fetch('http://localhost:8000/productos/' + id, {
            method: 'PUT' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: product,
        });
    }

    async closeProduct(id){
        return fetch('http://localhost:8000/productos/' + id, {
            method: 'DELETE' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
}


export default ProductRequests;