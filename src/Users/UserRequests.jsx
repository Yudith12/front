
class UserRequests {
    constructor(){}


    async listUsers(){
        return fetch('http://localhost:8000/usuarios/', {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
    
    async createUser(user){
        return fetch('http://localhost:8000/usuarios/', {
            method: 'POST' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: user,
        });
    }
    
    async getUser(id){
        return fetch('http://localhost:8000/usuarios/' + id, {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }

    async updateUser(id, user){
        return fetch('http://localhost:8000/usuarios/' + id, {
            method: 'PUT' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
            body: user,
        });
    }

    async closeUser(id){
        return fetch('http://localhost:8000/usuarios/' + id, {
            method: 'DELETE' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        });
    }
}


export default UserRequests;