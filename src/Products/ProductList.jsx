import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { show_alerta } from '../functions';
import Peticiones from './ProductRequests';
import UserManager from '../Users/UserManager';

const ProductList = ({onClickUser}) => {

  const role = JSON.parse(window.localStorage.getItem('role'));
  const peticiones = new Peticiones();

  const url='http://localhost:8000/productos/';
  const [productos, setProductos]= useState([]);
  const [id, setId] = useState('');
  const [nombre, setNombre] = useState('');
  const [precio, setPrecio] = useState('');
  const [cantidad, setCantidad] = useState('');
  const [operation, setOperation] = useState(1);
  const [title, setTitle] = useState('');

      useEffect(() => {
        peticiones.listProducts()
            .then((res) => res.json())
            .then((data) => {
                setProductos(data)
            })
    }, [])

    const getProductos = async () => {
      try {
        const respuesta = 
        await axios({ 
          method:'GET',
          url:url, 
          headers: {
            Authorization: `Bearer ${JSON.parse(
              window.localStorage.getItem('accessToken')
            )}`,
            'Content-Type': 'application/json',
          },
         })
    
        setProductos(respuesta.data);
        console.log(respuesta);
      } catch (error) {
        // Manejo de errores
        console.error(error);
      }
    };

    const openModal = (op, id, nombre, precio, cantidad) => {
      setId('');
      setNombre('');
      setPrecio('');
      setCantidad('');
      setOperation(op);
      if (op === 1) {
        setTitle('Registrar Producto');        
      }else if(op === 2){
        setTitle('Editar Producto'); 
        setId(id);
        setNombre(nombre);
        setPrecio(precio);
        setCantidad(cantidad);       
      }
      window.setTimeout(function(){
        document.getElementById('nombre').focus();
      },500);
    }

    const validar = () =>{
      var parametros;
      var metodo;

      if(nombre.trim()===''){
        show_alerta('Escribe el nombre del producto', 'warning');
      }else if(precio===''){
        show_alerta('Escribe el precio del producto', 'warning');
      }else if(cantidad===''){
        show_alerta('Escribe la cantidad disponible del producto', 'warning');
      }else{
        if (operation===1) {
          parametros = {nombre:nombre.trim(), precio:precio, cantidad:cantidad};
          metodo = 'POST';
        }else{
          parametros = {id:id, nombre:nombre.trim(), precio:precio, cantidad:cantidad};
          metodo = 'PUT';
        }

        enviarSolicitud(metodo, parametros);

      }
    }

    const enviarSolicitud = async(metodo, parametros) => {
      var editar = metodo=='POST'? '' : (parametros.id + '/');
      let mensaje;
      
      // var mensaje = metodo=='POST'? 'Agregado correctamente' : 'Modificado correctamente';

      if (metodo === 'POST') {
        mensaje = 'Agregado correctamente';
      } else if (metodo === 'PUT') {
        mensaje = 'Modificado correctamente';
      } else if (metodo === 'DELETE') {
        mensaje = 'Eliminado correctamente';
      }

      await axios({ 
         method:metodo,
         url:url + editar, 
         data:parametros,
         headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
        },
        })

        .then(function (respuesta) {
          console.log('respuesta de la peticion= '+ JSON.stringify(respuesta.data));
          var tipo =  'success';
          show_alerta(mensaje, tipo);
  
          if(tipo==='success'){
            document.getElementById('btnCerrar').click();
            getProductos();
          }
        })
        .catch(function(error){
          show_alerta('Error en la solicitud', 'error');
          // console.log(error);
        });
      // }
    }


    const deleteProducto = (id, nombre) =>{
      const MySwal = withReactContent(Swal);
      MySwal.fire({
        title: 'Seguro que desea eliminar el producto' + nombre + '?',
        icon: 'question',
        text:'No se podrá dar marcha atrás',
        showCancelButton: true,
        confirmButtonText: 'Si, deseo eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) =>{
        if(result.isConfirmed){
          setId(id);
          enviarSolicitud('DELETE', {id:id});
        }else{
          show_alerta('El producto no fue eliminado', 'info');
        }
      })
    }

    const createButton = () => {
      if( role == 'admin'){
          return <button className="btn btn-primary" onClick={() => onClickUser()}>Usuarios</button>
      }else{
          return <br/>
      }
    }

  return (
    
    <div className='App'>
      <div className="position-absolute start-10">{createButton()}</div>
        <br/><br/>
      <div className='container-fluid'>
          <div className='row mt-3'>
            <div className='col-md-4 offset-md-4'>
              <div className='d-grid mx-auto'>
              <h1>Productos</h1>
                <button onClick={()=>openModal(1)} className='btn btn-dark' data-bs-toggle='modal' data-bs-target='#modalProductos'>
                    <i className='fa-solid fa-circle-plus'></i> Añadir
                </button>
              </div>
            </div>
          </div>
          <div className='row mt-3'>
              <div className='col-12 col-lg-12 offset-0 offset-lg-2'>
                  <div className='table-responsive'>
                    <table className='table table-bordered'>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>PRODUCTO</th>
                          <th>PRECIO</th>
                          <th>CANTIDAD</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody className='table-ggroup-divider'>
                        {productos.map((producto)=>(
                          <tr key={producto.id}>
                            <td>{producto.id}</td>
                            <td>{producto.nombre}</td>
                            <td>{producto.precio}</td>
                            <td>{producto.cantidad}</td>
                            <td>
                              <button  onClick={()=>openModal(2, producto.id, producto.nombre, producto.precio, producto.cantidad)} className='btn btn-warning' data-bs-toggle='modal' data-bs-target='#modalProductos'>
                                <i className='fa-solid fa-edit'></i>
                              </button>
                              &nbsp;
                              <button className='btn btn-danger' onClick={()=>deleteProducto(producto.id, producto.nombre)}>
                                <i className='fa-solid fa-trash'></i>
                              </button>
                            </td>
                          </tr>
                        ))

                        }

                      </tbody>
                    </table>
                  </div>
              </div>
          </div>

      </div>

      <div id='modalProductos' className='modal fade' aria-hidden='true'>
        <div className='modal-dialog'>
            <div className='modal-content'>
              <div className='modal-header'>
                <label className='h5'>
                  {title}
                </label>
                <button type='button' className='btn-close' data-bs-dismiss='modal' id='btnCerrar' aria-label='Close'></button>
              </div>
              <div className='modal-body'>
                  <input type="hidden" id='id'></input>

                  <div className='input-group mb-3'>
                    {/* <span className='input-group-text'><i className="fa-solid fa-mug-hot"></i></span> */}
                    <input type="text" id='nombre' className='form-control' placeholder='Nombre' value={nombre} onChange={(e)=>setNombre(e.target.value)}></input>
                  </div>
                  <div className='input-group mb-3'>
                    {/* <span className='input-group-text'><i className="fa-solid fa-dollar-sign"></i></span> */}
                    <input type="text" id='precio' className='form-control' placeholder='Precio' value={precio} onChange={(e)=>setPrecio(e.target.value)}></input>
                  </div>
                  <div className='input-group mb-3'>
                    {/* <span className='input-group-text'><i class="fa-solid fa-cubes-stacked"></i></span> */}
                    <input type="text" id='cantidad' className='form-control' placeholder='Cantidad' value={cantidad} onChange={(e)=>setCantidad(e.target.value)}></input>
                  </div>
                  <div className='d-grid col-6 mx-auto'>
                      <button className='btn btn-success' onClick={() =>validar()}><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                  </div>
              </div>
              <div className='modal-footer'>
                 <button type='button' className='btn btn-secondary'  data-bs-dismess='modal' aria-label='Close'>Cerrar</button>
              </div>

            </div>
        </div>    
          
      </div>
      
    </div>
  )
}

export default ProductList
