import React, { useState } from 'react';

const ProductTable = ({ selectedProduct , handleProductsArrayChange}) => {

  let res = 0; // sumatoria del total a mostrar
  
  const [productArray, setProductArray] = useState([]);

  const addProductToTable = (product) => {
    if (!productArray.find((p) => p.id === product.id) && product) {
      setProductArray([...productArray, { ...product, cantidad: 1 }]);
    }
  };

  const handleQuantityChange = (index, newQuantity) => {
    setProductArray((prevState) => {
      const updatedProductArray = [...prevState];
      updatedProductArray[index].cantidad = newQuantity;
      return updatedProductArray;
    });
  };

  const handleRemoveItem = (index) => {
    setProductArray((prevState) => {
      const newdProductArray = [...prevState];
      newdProductArray.splice(index, 1);
      return newdProductArray;
    });
  }

  addProductToTable(selectedProduct);
  // console.log('Productossss = ');
  console.log(JSON.stringify(productArray));
  handleProductsArrayChange(productArray);

  const TotalPedido = () => {
    let sum = 0;
    productArray.forEach(producto => {
      sum += (producto.precio * producto.cantidad);
    });
    return sum;
  };

  return (
    <div className='table-responsive'>
      <br />
      <br />
      {selectedProduct && (
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Precio</th>
              <th>Cantidad</th>
            </tr>
          </thead>
          <tbody>
            {productArray.map((product, index) => (
              <tr key={index}>
                <td>{product.nombre}</td>
                <td>{product.precio}</td>
                <td className="d-flex align-items-center">
                  
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => handleQuantityChange(index, product.cantidad - 1)}
                    >
                      -
                    </button>
                    <input
                      type='number'
                      className="form-control form-control-sm px-1 py-0"
                      value={product.cantidad}
                      onChange={(e) =>
                        handleQuantityChange(index, parseInt(e.target.value))
                      }
                    />
                    <button
                      type="button"
                      className="btn btn-primary"
                      onClick={() => handleQuantityChange(index, product.cantidad + 1)}
                    >
                      +
                    </button>
                    
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => handleRemoveItem(index)}
                    >
                      <i class="fa-solid fa-trash-can"></i>
                    </button>
                </td>

                {/* <td className="d-flex align-items-center">
                  <div className="input-group mb-3">
                      <div className="input-group mb-3">
                          <button className="btn btn-outline-primary" type="button" onClick={() => handleQuantityChange(index, product.cantidad - 1)}>-</button>
                          <input type='number' className="form-control form-control-sm px-1 py-0" onChange={(e) =>
                        handleQuantityChange(index, parseInt(e.target.value))
                      } value={product.cantidad}/>
                          <button className="btn btn-outline-primary" type="button"  onClick={() => handleQuantityChange(index, product.cantidad + 1)}>+</button>
                      </div>
                  </div>
                </td> */}
              </tr>
            ))}
            <tr>
              <td colSpan="2">Total</td>
              <td>{TotalPedido()}</td>
            </tr>
          </tbody>
        </table>
      )}
    </div>
  );
};

export default ProductTable;
