import { useState, useEffect } from 'react'
// import Productos from './Productos'
import ProductManager from './Products/ProductManager';
import UserManager from './Users/UserManager';
import OrderForm from './Order/Form/OrderForm';
import Pedidos from './Pedidos'
import OrderManager from './Order/OrderManager';

const Home = ({ onLogout, userId }) => {
  const [user, setUser] = useState()

  useEffect(() => {
    fetch('http://localhost:8000/usuarios/' + userId, {
      method: 'GET' /* or POST/PUT/PATCH/DELETE */,
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json'
      },
    })
      .then((res) => res.json())
      .then((userData) => {
        window.localStorage.setItem('role', JSON.stringify(userData.group_name))
        setUser(userData)
      })
      // .catch( (error) => console.log( error))
  }, [])

  const logoutHandler = () => {
    onLogout()
  }

  const role = user ? user.group_name : null

  // const content = role && role === 'recepcion' ? <Productos/> : <Pedidos />
  const content = role && role === 'admin' ? <UserManager/> : <OrderManager />
  // const content =  <OrderManager /> 
  return (
    <>
      <button onClick={logoutHandler}>Logout</button>
      {user && <>
        <h1>Bienvenido/a {user.username}!</h1>
        {/* <p>{user.group_name}</p> */}
        <br/><br/><br/>
        {content}
      </>}
    </>
  )
}

export default Home
