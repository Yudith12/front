import { useState, useEffect } from 'react'
import OrderRequests from './Order/OrderRequests'

const orderRequest = new OrderRequests();

const Home = () => {
    const [Pedidos, setPedidos] = useState([])

    useEffect(() => {
            orderRequest.listOrders()
            .then((res) => res.json())
            .then((data) => {
                setPedidos(data)
            })
    }, [])

    return (
        <>
            <h2>Pedidos</h2>
            <div key="pedidosAccordion" className="accordion" id="pedidos">
                {Pedidos.map((pedido) => {
                    console.log("EL PEDIDO \n" + JSON.stringify(pedido));
                    let entrada = new Date(pedido.entrada);
                    let targetAccordionButton = "#" + pedido.id;
                    let productos = JSON.parse(pedido.productos);
                    console.log(productos);
                    return (<>
                        <div className="accordion-item">
                            <h2 className="accordion-header">
                            <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={targetAccordionButton} aria-expanded="false" aria-controls={pedido.id}>
                                {pedido.cliente} - {entrada.getUTCHours()}:{entrada.getMinutes()}:{entrada.getSeconds()}
                            </button>
                            </h2>
                            <div id={pedido.id} className="accordion-collapse collapse" data-bs-parent="#pedidos">
                                <div className="accordion-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">Producto</th>
                                            <th scope="col">Cantidad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {productos.map((producto) => {
                                            return (<>
                                            <tr>
                                                <td>{producto.name}</td>
                                                <td>{producto.cantidad}</td>
                                            </tr>
                                                </>
                                            );
                                            
                                        })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </>
                    );
                })}
            </div>
        </>
    )
}

export default Home
