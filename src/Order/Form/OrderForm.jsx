import React, { useState } from 'react';
import Peticiones from '../OrderRequests';
import ProductSelect from './ProductSelect';
import ProductTable from './ProductTable';
import axios from 'axios';



const OrderForm = () => {

  const peticiones = new Peticiones();

  const url='http://localhost:8000/pedidos/';

  const [cliente, setCliente] = useState('');
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [productsArray, setProductsArray] = useState(null);

  // console.log('productos que se piden = ' + selectedProduct);
  console.log("EL ARRAY DE PRODUCTOS " + JSON.stringify(productsArray));
  // console.log('HASTA ACA= ');

  const handleSave = () => {
    const orderData = {
      cliente: cliente,
      // productos: selectedProduct ? [JSON.stringify(selectedProduct)] : [],
      productos: productsArray,
      estado: 'cocina',
      salida: null,
      tiempo_total: null
    };

    console.log("Order Data "+ JSON.stringify(orderData));
      try {
        const respuesta = 
          axios({ 
          method:'POST',
          url:url, 
          data: orderData,
          headers: {
            Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
            'Content-Type': 'application/json',
          },
        })
        .then( (res) => console.log(res))
        .then(() => {
          // Restablecer los valores del formulario
          setCliente('');
          setSelectedProduct(null);
        })
      } catch (error) {
        // Manejo de errores
        console.error(error);
      }

  };
  
  const handleCancel = (e) => {
    e.preventDefault();
    console.log("Se impidio la navegacion");
    setCliente('');
    setSelectedProduct('');
    setProductsArray(null);
  }

  return (
    <form onSubmit={handleSave}>
      <input
        type="text"
        placeholder="Nombre del Cliente"
        value={cliente}
        onChange={(e) => setCliente(e.target.value)}
      />
      <div>
        <ProductSelect handleProductChange={setSelectedProduct} />
        <ProductTable 
          selectedProduct={selectedProduct} 
          handleProductsArrayChange={setProductsArray} />
      </div>
      <div className="container">
        <div className="button-container">
          <button className='button' onClick={() => handleCancel} >Cancelar</button>
          <button className='button' type="submit" >Guardar</button>
        </div>
      </div> 
    </form>
  );
};

export default OrderForm;
