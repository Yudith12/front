import React, {useEffect, useState} from 'react';
import axios from 'axios';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';
import { show_alerta } from '../functions';
import Peticiones from './UserRequests';
import ProductManager from '../Products/ProductManager';

const UserList = ({onClickProduct}) => {
  const role = JSON.parse(window.localStorage.getItem('role'));
  const peticiones = new Peticiones();

  const url='http://localhost:8000/usuarios/';
  const [usuarios, setUsuarios]= useState([]);
  const [id, setId] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [first_name, setFirstname] = useState('');
  const [last_name, setLastname] = useState('');
  const [group_name, setGroupname] = useState('');
  const [operation, setOperation] = useState(1);
  const [title, setTitle] = useState('');

      useEffect(() => {
        peticiones.listUsers()
            .then((res) => res.json())
            .then((data) => {
                setUsuarios(data)
            })
    }, [])

    const getUsuarios = async () => {
      try {
        const respuesta = 
        await axios({ 
          method:'GET',
          url:url, 
          headers: {
            Authorization: `Bearer ${JSON.parse(
              window.localStorage.getItem('accessToken')
            )}`,
            'Content-Type': 'application/json',
          },
         })
    
        setUsuarios(respuesta.data);
        // console.log(respuesta);
      } catch (error) {
        // Manejo de errores
        console.error(error);
      }
    };

    const openModal = (op, id, username, password, email, group_name) => {
      setId('');
      setUsername('');
      setPassword('');
      setEmail('');
      setGroupname('');
      setOperation(op);
      if (op === 1) {
        setTitle('Registrar Usuario');        
      }else if(op === 2){
        setTitle('Editar Usuario'); 
        setId(id);
        setUsername(username);
        setPassword(password);
        setEmail(email);
        setGroupname(group_name);       
      }
      window.setTimeout(function(){
        document.getElementById('username').focus();
      },500);
    }

    const validar = () =>{
      var parametros;
      var metodo;

      if(username.trim()===''){
        show_alerta('Escribe el usuario', 'warning');
      }else if(password===''){
        show_alerta('Escribe la contraseña', 'warning');
      }else if(group_name===''){
        show_alerta('Escribe el rol para este usuario', 'warning');
      }else{
        if (operation===1) {
          parametros = {username:username.trim(), password:password, email:email, group_name:group_name};
          metodo = 'POST';
        }else{
          parametros = {id:id, username:username.trim(), password:password, email:email, group_name:group_name};
          metodo = 'PUT';
        }

        enviarSolicitud(metodo, parametros);

      }
    }

    const enviarSolicitud = async(metodo, parametros) => {
      var editar = metodo=='POST'? '' : (parametros.id);
      let mensaje;
      
      // var mensaje = metodo=='POST'? 'Agregado correctamente' : 'Modificado correctamente';

      if (metodo === 'POST') {
        mensaje = 'Agregado correctamente';
      } else if (metodo === 'PUT') {
        mensaje = 'Modificado correctamente';
      } else if (metodo === 'DELETE') {
        mensaje = 'Eliminado correctamente';
      }

      await axios({ 
         method:metodo,
         url:url + editar, 
         data:parametros,
         headers: {
          Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
          'Content-Type': 'application/json',
        },
        })

        .then(function (respuesta) {
        //   console.log('respuesta de la peticion= '+ JSON.stringify(respuesta.data));
          var tipo =  'success';
          show_alerta(mensaje, tipo);
  
          if(tipo==='success'){
            document.getElementById('btnCerrar').click();
            getUsuarios();
          }
        })
        .catch(function(error){
          show_alerta('Error en la solicitud', 'error');
          // console.log(error);
        });
      // }
    }


    const deleteUsuario = (id, username) =>{
      const MySwal = withReactContent(Swal);
      MySwal.fire({
        title: 'Seguro que desea eliminar el usuario ' + username + '?',
        icon: 'question',
        text:'No se podrá dar marcha atrás',
        showCancelButton: true,
        confirmButtonText: 'Si, deseo eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) =>{
        if(result.isConfirmed){
          setId(id);
          enviarSolicitud('DELETE', {id:id});
        }else{
          show_alerta('El usuario no fue eliminado', 'info');
        }
      })
    }


    const createButton = () => {
      if( role == 'admin'){
          return <button className="btn btn-primary" onClick={() => onClickProduct()}>Productos</button>
      }else{
          return <br/>
      }
    }
  return (
    <div className='App'>

<div className="position-absolute start-10">{createButton()}</div>
        <br/><br/>
      <div className='container-fluid'>
          <div className='row mt-3'>
            <div className='col-md-4 offset-md-4'>
              <div className='d-grid mx-auto'>
                <h1>Usuarios</h1>
                <button onClick={()=>openModal(1)} className='btn btn-dark' data-bs-toggle='modal' data-bs-target='#modalUsuarios'>
                    <i className='fa-solid fa-circle-plus'></i> Añadir
                </button>
              </div>
            </div>
          </div>
          <div className='row mt-3'>
              <div className='col-12 col-lg-12 offset-0 offset-lg-2'>
                  <div className='table-responsive'>
                    <table className='table table-bordered'>
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>USUARIO</th>
                          <th>EMAIL</th>
                          <th>ROL</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody className='table-ggroup-divider'>
                        {usuarios.map((usuario)=>(
                          <tr key={usuario.id}>
                            <td>{usuario.id}</td>
                            <td>{usuario.username}</td>
                            <td>{usuario.email}</td>
                            <td>{usuario.group_name}</td>
                            <td>
                              <button  onClick={()=>openModal(2, usuario.id, usuario.username, usuario.password, usuario.email, usuario.group_name)} className='btn btn-warning' data-bs-toggle='modal' data-bs-target='#modalUsuarios'>
                                <i className='fa-solid fa-edit'></i>
                              </button>
                              &nbsp;
                              <button className='btn btn-danger' onClick={()=>deleteUsuario(usuario.id, usuario.username)}>
                                <i className='fa-solid fa-trash'></i>
                              </button>
                            </td>
                          </tr>
                        ))

                        }

                      </tbody>
                    </table>
                  </div>
              </div>
          </div>

      </div>

      <div id='modalUsuarios' className='modal fade' aria-hidden='true'>
        <div className='modal-dialog'>
            <div className='modal-content'>
              <div className='modal-header'>
                <label className='h5'>
                  {title}
                </label>
                <button type='button' className='btn-close' data-bs-dismiss='modal' id='btnCerrar' aria-label='Close'></button>
              </div>
              <div className='modal-body'>
                  <input type="hidden" id='id'></input>

                  <div className='input-group mb-3'>
                    <input type="text" id='username' className='form-control' placeholder='User name' value={username} onChange={(e)=>setUsername(e.target.value)}></input>
                  </div>
                  <div className='input-group mb-3'>
                    <input type="text" id='setPassword' className='form-control' placeholder='Contraseña' value={password} onChange={(e)=>setPassword(e.target.value)}></input>
                  </div>
                  <div className='input-group mb-3'>
                    <input type="text" id='email' className='form-control' placeholder='Email' value={email} onChange={(e)=>setEmail(e.target.value)}></input>
                  </div>
                  <div className="input-group mb-3">
                    <select
                      id="group_name"
                      className="form-select"
                      value={group_name}
                      onChange={(e)=>setGroupname(e.target.value)}
                    >
                      <option value="">Selecciona un Rol</option>
                      <option value="cocina">Cocina</option>
                      <option value="recepcion">Recepción</option>
                      <option value="admin">Administración</option>
                    </select>
                  </div>
                  <div className='d-grid col-6 mx-auto'>
                      <button className='btn btn-success' onClick={() =>validar()}><i class="fa-solid fa-floppy-disk"></i> Guardar</button>
                  </div>
              </div>
              <div className='modal-footer'>
                 <button type='button' className='btn btn-secondary'  data-bs-dismess='modal' aria-label='Close'>Cerrar</button>
              </div>

            </div>
        </div>    
          
      </div>

      
    </div>
  )
}

export default UserList
