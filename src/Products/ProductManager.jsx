import React, { useState, useEffect } from 'react';
import ProductForm from './ProductForm';
import ProductList from './ProductList';
import UserManager from '../Users/UserManager';

const ProductManager = () => {
  const [productos, setProductos] = useState([]);

  const role = JSON.parse(window.localStorage.getItem('role'));
  const [showUserManager, setShowUserManager] = useState(false);

  const handleClick = () => {
    setShowUserManager(true);
  };


  useEffect(() => {
    fetch('http://localhost:8000/productos/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setProductos(data);
      });
  }, []);

  const handleSaveProducto = (newProducto) => {
    // Guardar o actualizar el producto en la base de datos
    fetch('http://localhost:8000/productos/', {
      method: 'POST', // o PUT si es una actualización
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newProducto),
    })
      .then((res) => res.json())
      .then((data) => {
        // Actualizar la lista de productos con el nuevo producto o la actualización
        setProductos([...productos, data]);
      });
  };

  const handleDeleteProducto = (productoId) => {
    // Eliminar el producto de la base de datos
    fetch(`http://localhost:8000/productos/${productoId}`, {
      method: 'DELETE',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then(() => {
        // Eliminar el producto de la lista de productos
        const updatedProductos = productos.filter((producto) => producto.id !== productoId);
        setProductos(updatedProductos);
      });
  };

  const content = (role === 'admin' && showUserManager ) ? <UserManager /> : <ProductList productos={productos}  onClickUser={handleClick} /> ;

  return (
    <div>
      {content}
    </div>
  );
};

export default ProductManager;
