import React, { useState, useEffect } from 'react';
// import UserForm from './UserForm';
import UserList from './UserList';
import ProductManager from '../Products/ProductManager';

const UserManager = () => {
  const [usuarios, setUsuarios] = useState([]);


    const role = JSON.parse(window.localStorage.getItem('role'));
  const [showProductManager, setShowProductManager] = useState(false);

  const handleClick = () => {
    setShowProductManager(true);
  };

  useEffect(() => {
    fetch('http://localhost:8000/usuarios/', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUsuarios(data);
      });
  }, []);

  const handleSaveProducto = (newProducto) => {
    // Guardar o actualizar el producto en la base de datos
    fetch('http://localhost:8000/usuarios/', {
      method: 'POST', // o PUT si es una actualización
      headers: {
        Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(newProducto),
    })
      .then((res) => res.json())
      .then((data) => {
        // Actualizar la lista de usuarios con el nuevo producto o la actualización
        setUsuarios([...usuarios, data]);
      });
  };

  const content = (role === 'admin' && showProductManager ) ? <ProductManager /> : <UserList usuarios={usuarios}  onClickProduct={handleClick} /> ;


  return (
    <div>
      {/* <ProductForm onSave={handleSaveProducto} /> */}
      {/* <UserList usuarios={usuarios} /> */}
      {content}

    </div>
  );
};

export default UserManager;
