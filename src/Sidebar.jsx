import React from 'react'

function Sidebar() {
  return (
    <div className='container-fluid'>
      <div className='row'>
        <div className='bg-dark col-auto col-md-2 min-vh-100'>
          <a className='text-decoration-none text-white d-flex align-itemcenter ms-3 mt-2' href="">
            <i className='fs-4 bi bi-speedometer'></i> 
            <span className='ms-1 fs-a'>Menu</span>
          </a>
          <hr />
          <ul className="nav nav-pills flex-column">
              <li className="nav-item text-white fs-4">
                  <a href="#" className="nav-link text-white fs-5" aria-current="page"><i className="fa-solid fa-right-to-bracket"></i> <span className='ms-2'>Pedidos</span></a>
              </li>
              <li className="nav-item text-white fs-4">
                  <a href="#" className="nav-link text-white fs-5"> <i className="fa-solid fa-circle-user"></i> <span className='ms-2'>Usuarios</span></a>
              </li>
              <li className="nav-item text-white fs-4">
                  <a href="#" className="nav-link text-white fs-5"><i className="fa-solid fa-mug-hot"></i><span className='ms-2'>Productos</span></a>
              </li>
          </ul>
        </div>
      </div>
      
    </div>
  )
}

export default Sidebar
